from django.shortcuts import render

# Create your views here.

from django.contrib.auth.forms import UserCreationForm
from django.urls import reverse_lazy
from django.views import generic


class SignUpView(generic.CreateView):
	form_class = UserCreationForm 			# the form of class SignUpView takes from UserVreationForm	
	success_url = reverse_lazy('login') 	# if it's successful? -> 'login.html'
	template_name = 'signup.html'			